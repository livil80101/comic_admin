<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>靜態登入頁面</title>
</head>
<body class="">

<form action="" method="post">
    <label>帳號</label>
    <br/>
    <input type="text" placeholder="帳號" value="AAAA" name="account">
    <br/>
    <label>密碼</label>
    <br/>
    <input type="password" placeholder="密碼" value="AAAA" name="password">
    <br>
    {{ csrf_field() }}
    <input type="submit" value="靜態登入">
</form>
{{-- 這邊放錯誤訊息 --}}
@if($errors->any())
    <ul>
        @foreach($errors->all()  as $error)

            <li>{{ $error }}</li>

        @endforeach
    </ul>
@endif
</body>
</html>
