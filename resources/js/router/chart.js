import Layout from '../views/Layout';
import list from "../views/chart/List";
import MyWeight from "../views/chart/MyWeight";

export default {
    path     : "/chart",
    component: Layout,
    children : [
        {
            path     : "",
            component: list,
            name     : 'chart.list',
            meta     : {title: '圖表'}
        },

        {
            path     : "my_weight",
            component: MyWeight,
            name     : 'chart.my_weight',
            meta     : {title: '體重記錄'}
        },

    ]
};
