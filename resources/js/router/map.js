import Layout from "../views/Layout";
import list from "../views/map/list";

import LeafletDefault from "../views/map/leaflet_default";

export default {
    path     : "/map",
    component: Layout,
    children : [
        {
            path     : "",
            component: list,
            name     : 'map.list',
            meta     : {title: '地圖目錄'}
        },
        {
            path     : "leaflet/1",
            component: LeafletDefault,
            name     : 'map.leaflet.1',
            meta     : {title: '使用leaflet'}
        },

    ],
};
