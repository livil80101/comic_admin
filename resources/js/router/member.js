import Layout from "../views/Layout";
import list from "../views/member/list";
import info from "../views/member/info";
import roles from "../views/member/roles";
import permissions from "../views/member/permissions";
import search from "../views/member/search";
import excel from "../views/member/excel";

export default {
    path     : "/member",
    component: Layout,
    children : [
        {
            path     : "",
            component: list,
            name     : 'member.list',
            meta     : {title: '會員列表'}
        },

        {
            path     : "one/:id",
            component: info,
            name     : 'member.one',
            meta     : {title: '會員管理'}
        },

        {
            path     : "search",
            component: search,
            name     : 'member.search',
            meta     : {title: '會員搜尋'}
        },


        {
            path     : "roles",
            component: roles,
            name     : 'member.roles',
            meta     : {title: '身份管理'}
        },

        {
            path     : "permissions",
            component: permissions,
            name     : 'member.permissions',
            meta     : {title: '權限管理'}
        },

        {
            path     : "excel",
            component: excel,
            name     : 'member.excel',
            meta     : {title: 'excel匯出匯入'}
        },

    ],
};
