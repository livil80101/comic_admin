import Dashboard from "../views/Dashboard";
import Empty from '../config/Empty'
import Layout from "../views/Layout";

let r = [
    {
        path     : "/",
        component: Layout,
        children : [
            {
                // 404
                path: 'empty',
                name: 'empty',
                // component: require('./Empty'),
                component: Empty,
                meta     : {title: '404未知的路徑'}
            }, {
                path     : '',
                name     : 'root',
                component: Dashboard,
                meta     : {title: '首頁'}
            },
        ],
    },
    {
        // 404
        path: '*',
        name: 'not_found',
        // component: require('./Empty'),
        component: Empty,
        meta     : {title: '404未知的路徑'}
    },

]

//漫畫路徑
import comic from "./comic";

r.push(comic);

//會員管理路徑
import member from "./member";

r.push(member);

export default r;

//員工管理
import employee from "./employee";

r.push(employee);

//地圖功能

import map from "./map";

r.push(map);

//圖表
import chart from "./chart";

r.push(chart);
