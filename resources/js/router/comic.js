import comic from "../views/comic/comic";
import add from '../views/comic/add'
import Layout from '../views/Layout';
import detail from "../views/comic/detail";
import chapter from "../views/comic/chapter";
import list from "../views/comic/List";

export default {
    path     : "/comic",
    component: Layout,
    children : [
        {
            path     : "",
            component: list,
            name     : 'comic.list',
            meta     : {title: '漫畫列表'}
        },
        {
            path     : "c/:id",
            component: detail,
            name     : 'comic.detail',
            meta     : {title: '漫畫'}
        },
        {
            path     : "ch/:id",
            component: chapter,
            name     : 'comic.chapter',
            meta     : {title: '章節'}
        },
        {
            path     : "add",
            name     : 'comic.add',
            component: add,
            meta     : {title: '新增漫畫'}
        }
    ]
};
