import Layout from "../views/Layout";
import list from "../views/employee/list";
import list2 from "../views/employee/list2";
import info from "../views/employee/info";
import roles from "../views/employee/roles";

export default {
    path     : "/employee",
    component: Layout,
    children : [
        {
            path     : "",
            component: list2,
            name     : 'employee.list2',
            meta     : {title: '員工列表2'}
        },
        {
            path     : "list",
            component: list,
            name     : 'employee.list',
            meta     : {title: '員工列表'}
        },

        {
            path     : "one/:id",
            component: info,
            name     : 'employee.one',
            meta     : {title: '員工管理'}
        },

        {
            path     : "roles",
            component: roles,
            name     : 'employee.roles',
            meta     : {title: '身份管理'}
        },

    ],
};
