import Vue from 'vue';
import Vuex from 'vuex';
import axios from "axios";

Vue.use(Vuex);

const store = new Vuex.Store({
    //類似data
    state: {
        target: null,
        user  : null,
    },
    //類似function
    mutations: {
        setTarget(state, target) {
            state.target = target;
        }
    },
    //類似computed
    getters: {
        getTarget: (state) => state.target,
        getUser(state) {


            return state.user;
        }
    },
    actions: {

    },

    //
    modules: {}
})
export default store;
