import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
    //類似data
    state: {
        target: null,
        user  : null,
    },
    //類似function
    mutations: {
        setTest(state, target) {
            state.target = target;
        },
        setUser(state, user) {
            state.user = user;
        },
        deleteUser(state) {
            state.user = null;
        },
    },
    //類似computed
    getters: {
        getTest: (state) => state.target,
        getUser(state) {
            return state.user;
        }
    },

    //命名空間
    modules: {
        comic_chapter: {
            namespaced: true,
            state     : {
                mode      : 0,
                test      : 1,
                pages     : [],
                show_page : [],
                show_count: 12,
            },
            mutations : {
                setMode(state, mode) {
                    state.mode = mode;
                },
                changeMode(state, event) {
                    state.mode = event.target.value;
                },
                setPages(state, pages) {
                    state.pages = pages;
                }
            },
            getters   : {
                getMode         : (state) => state.mode,
                getPages        : (state) => state.pages,
                showPages       : (state) => state.show_page,
                getShowPageCount: (state) => state.show_count,
                allMode         : () => [
                    {mode: 0, text: '排序/新增'},
                    {mode: 1, text: '大張閱讀模式'},
                ]
            }

        }
    }
})
export default store;
