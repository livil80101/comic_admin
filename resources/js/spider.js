import axios from "axios";
import Vue from "vue";
import ElementUI from 'element-ui';

//這邊還是使用VUE吧
import Spider from "./Spider/spider" ;
import store from './Store/spider';

Vue.use(ElementUI);

document.addEventListener("DOMContentLoaded", function () {
    console.log("喵");
    new Vue({
        el        : "#app",
        components: {
            Spider
        },
        store
    });
});
