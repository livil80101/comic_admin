require('./bootstrap');

import Vue from "vue";

//這邊還是使用VUE吧
import App from "./views/App";
import store from './Store/store';

import VueRouter from "vue-router";

Vue.use(VueRouter);


import ElementUI from 'element-ui';
import local from "element-ui/lib/locale/lang/zh-TW";

Vue.use(ElementUI, {local});

import router from './router/router';


import matomo from 'vue-matomo'

Vue.use(matomo, {
    host  : 'http://matomo.weil.com.tw', // 這裡配置你自己的piwik服務器地址和網站ID
    siteId: 1,//siteId值
    // 根據router自動註冊
    router: router,
    // 是否需要在發送追蹤信息之前請求許可
    // 默認false
    requireConsent    : false,
    enableLinkTracking: true,
    // 是否追蹤初始頁面
    // 默認true
    trackInitialView: false,
    // 最終的追蹤js文件名
    // 默認 'piwik'
    trackerFileName: 'matomo',
    debug          : false
});

document.addEventListener("DOMContentLoaded", function () {
    console.log("喵");
    new Vue({
        el        : "#app",
        components: {
            App
        },
        store,
        router
    });
});
