<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//爬蟲API
Route::prefix('spider')->group(function () {
    Route::get("/", "SpiderController@index");

    //抓取info
    Route::get("get_info_by_url", "SpiderController@info");
    Route::post("get_info_by_url", "SpiderController@info");
    //抓取分頁預覽
    Route::get("pv", "SpiderController@getPreviewByNumber");
    Route::post("pv", "SpiderController@getPreviewByNumber");
    //抓取封面
    Route::get("show_img", "SpiderController@showImage");
    //抓取單頁
    Route::get("get_page", "SpiderController@getPage");
    Route::post("get_page", "SpiderController@getPage");

    Route::get("test", "SpiderController@test");
});

//KKBOX
Route::prefix('KKBox')->group(function () {
    Route::get("/", "KKBoxController@index");
});

//漫畫
Route::get("api/comic", "ComicController@index");
Route::get('api/comic/detail/{id}', "ComicController@detail");
Route::get('api/comic/chapter/{id}', "ComicController@chapter");
Route::post('api/comic/chapter/{id}/new_sort', "ComicController@newSort");
Route::delete('api/comic/page/{id}', 'ComicController@deletePage');

//測試區

//Route::get("test", "TestController@index");
Route::get("test", 'TestController@index')->name('test');
Route::get("test/1", "TestController@a");
Route::get("test/2", "TestController@b");
Route::any("ajx/test", "TestController@ajaxTest");
Route::any("matomo/test", "TestController@matomo");
Route::get('/', function () {
    return view('welcome');
});


//自己的資料

//員工管理
Route::middleware("role:AA")->group(function () {//這邊放中介層 hr
    //員工列表
    Route::get("api/employee/list", "EmployeeController@employeeList");
    Route::get("api/employee/all_list", "EmployeeController@employeeDataList");
    Route::post("api/employee/all_list/{id}/detail", "EmployeeController@updateEmployeeDetail");
    Route::post("api/employee/all_list/{id}/roles", "EmployeeController@updateEmployeeRoles");

    //員工資料
    Route::get("api/employee/data/{id}", "EmployeeController@employeeData");
    Route::post("api/employee/data/{id}", "EmployeeController@employeeDataEdit");

    //員工身份管理
    Route::get("api/employee/roles", "EmployeeController@roles");
    Route::post("api/employee/roles", "EmployeeController@addRole");
    Route::put("api/employee/roles/{id}", "EmployeeController@updateRole");
    Route::delete("api/employee/roles/{id}", "EmployeeController@deleteRole");


    Route::get("api/create_employee", "EmployeeController@createEmployee");
    Route::post("api/create_employee", "EmployeeController@createEmployee");
});


//會員管理
Route::get("api/member", "MemberController@index");
Route::get("api/rolesPermissions", "MemberController@rolesPermissions");
Route::post("api/rolesPermissions", "MemberController@saveRolesPermissions");
Route::post("api/roles/add", "MemberController@addRole");
Route::delete("api/roles/delete", "MemberController@deleteRole");

//登入
Route::get("user", "AuthController@checkUser")->name("user");
Route::post("login", "AuthController@login")->name("login");
Route::get("login/static", "AuthController@pageLogin")->name("login.static");
Route::post("login/static", "AuthController@loginByPasswordStatic");
Route::get("logout", "AuthController@logout")->name("logout");
Route::get("logout/static", "AuthController@logoutStatic")->name("logout.static");
Route::middleware(['auth.json'])->group(function () {
    Route::get("login/test", "AuthController@test");
});

//圖表
Route::get("api/chart/my_weight", 'ChartController@myWeight');

Route::get('csrf', "IndexController@csrf")->name("csrf");

Route::get('/', "IndexController@index")->name("root");
Route::get('{any}', "IndexController@index")->where(["any" => ".*"]);
