<?php
return [
    "id"    => 1,
    "url"   => env("MATOMO_URL", "http://matomo.weil.com.tw"),
    "token" => env("MATOMO_TOKEN", ''),
];
