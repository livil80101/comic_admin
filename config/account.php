<?php
return [
    "root_account"  => env("ROOT_ACCOUNT"),
    "root_password" => env("ROOT_PASSWORD"),
    "root_email"    => env("ROOT_EMAIL"),
];
