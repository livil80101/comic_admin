<?php
return [
    'igneous'       => env("igneous", null),
    'ipb_member_id' => env("ipb_member_id", null),
    'ipb_pass_hash' => env("ipb_pass_hash", null),
    'sk'            => env("sk", null),
    'sl'            => env("sl", null),
];
