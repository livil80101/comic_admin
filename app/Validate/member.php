<?php


namespace App\Validate;


class member
{
    //修溝身份的介紹與權限
    const SAVE_ROLES_PERMISSIONS = [
        'id'          => 'int|string|required',
        'description' => 'string',
        'permissions' => 'array|exists:permissions',
    ];


}
