<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Validate\member as memberValidate;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;


class MemberController extends Controller
{
    public function index(Request $request)
    {
//        $limit = $request->input("limit", 10);
        $limit = 12;

        $re = User::with(["roles", "roles.permissions"])->paginate($limit)->toArray();

        return response()->json(
            $re
            , 200, [], JSON_UNESCAPED_SLASHES
        );
    }

    public function update(Request $request)
    {
        //這邊是修改 user info

    }

    public function rolesPermissions()
    {
        $roles       = Role::with([
            'permissions' => function (BelongsToMany $q) {
                $q->select("id");
            }
        ])->where("type", Role::MEMBER)->get();
        $permissions = Permission::where("type", Permission::MEMBER)->get();

        return response()->json([
            'roles'       => $roles,
            'permissions' => $permissions
        ]);
    }

    //修改
    public function saveRolesPermissions(Request $request)
    {
        //輸入驗證交給之後寫

        $id          = $request->input("id");
        $description = $request->input("description") ?? "";
        $permissions = $request->input("permissions", []);

        //權限部分驗證要比對資料庫
        try {
            $this->validate($request, memberValidate::SAVE_ROLES_PERMISSIONS);
        } catch (ValidationException $ex) {

            return response()->json([
                'input' => $request->all(),
                'error' => $ex->errors(),
            ], 400);
        }

        $r              = Role::find($id);
        $r->description = $description;
        $r->save();

        $r->permissions()->sync($permissions);

        return response()->json([
            'input' => $request->all()
        ]);
    }

    //新增 role
    public function addRole(Request $request)
    {
        $name         = $request->input("name", '');
        $display_name = $request->input("display_name", '');

        $has = Role::where("name", $name)->orWhere("display_name", $display_name)->get();

        if ($has->count() > 0) {
            return response()->json([
                'message' => '數據已經存在',
                'input'   => $request->all(),

            ], 400);
        }

        $role               = new Role();
        $role->name         = $name;
        $role->display_name = $display_name;
        $role->description  = '';
        $role->save();

        return response()->json([
            "input" => $request->all(),
            'role'  => $role,
            'db'    => []
        ]);
    }

    public function deleteRole(Request $request)
    {
        $id = $request->input("id");

        $role = Role::where("id", $id)->with("permissions")->first();

        if (!$role) {
            return response()->json([], 400);
        }

        $role->permissions()->sync([]);
        $role->delete();

        return response()->json([]);
    }
}
