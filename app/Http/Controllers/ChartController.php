<?php

namespace App\Http\Controllers;

use App\Models\MyWeight;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function myWeight()
    {
        $data = MyWeight::orderBy('created_at', 'desc')
            ->selectRaw(
                "id," .
                "weight," .
                "absorb_calories," .
                "consume_calories," .
                "year(created_at) as year, " .
                "month(created_at) as month , " .
                "day(created_at) as day , " .
                "hour(created_at) as hour "
            )->limit(20)->get();

        return response()->json([
            'data' => $data,
        ]);
    }
}
