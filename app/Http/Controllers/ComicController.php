<?php

namespace App\Http\Controllers;

use App\Models\Comic;
use App\Models\ComicChapter;
use App\Models\ComicPage;
use App\Models\SpiderPreView;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ComicController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->input('keyword');
        $tag     = $request->input('tag');

        $page = $request->input('page');

        //如果都沒有的話就直接回傳全部
        if (!($keyword || $tag))
            return response()->json(
                Comic::orderBy("id", 'desc')->paginate(12),
                200, [], JSON_UNESCAPED_SLASHES
            );

        $comic = Comic::orderBy("id", 'desc');

        if ($keyword) {
            $array_keyword = explode(',', $keyword);

            foreach ($array_keyword as $_keyword) {
                $comic->where('title', 'like', "%{$_keyword}%");
            }
        }

        //搜尋分類
        //先擱置
        if ($tag) {

        }

        return response()->json(
            $comic->paginate(12),
            200, [], JSON_UNESCAPED_SLASHES
        );
    }

    public function detail(Request $request, $id)
    {
        $comic = Comic::where('id', $id)->with([
            "chapters",
            "chapters.pages",
        ])->first();

        if ($comic)
            return response()->json($comic, 200, [], JSON_UNESCAPED_SLASHES);
        return response()->json([], 404);
    }

    public function chapter(Request $request, $id)
    {
        $chapter = ComicChapter::where('id', $id)
            ->with(["pages"])->first();

        if ($chapter) {
            return response()->json($chapter, 200, [], JSON_UNESCAPED_SLASHES);
        }
        return response()->json([], 404);
    }

    public function newSort(Request $request, $id)
    {
        //直接用for了
        $sort = $request->input("sort", []);

        if (is_array($sort) && count($sort) > 0) {
            foreach ($sort as $item) {
                ComicPage::where('id', $item['id'])->update([
                    "sort" => $item['sort']
                ]);
            }
        }
        //注意，可能會有 DB的寫入 寫出延遲問題，所以考慮一下前端自己解決
        return response()->json([
            'input' => $sort,

        ]);
    }

    public function deletePage(Request $request, $id)
    {
        //刪除指定頁面
        $page = ComicPage::find($id);
        //刪除快取
        SpiderPreView::where("storage_path", $page->path)->delete();
        $page->delete();
        if (File::exists($page->path)) {
            unlink($page->path);
        }

        return response()->json([
            'id'    => $id,
            "input" => $request->all(),
            "db"    => $page
        ]);
    }
}
