<?php

namespace App\Http\Controllers;

use App\Models\ComicChapter;
use App\Models\ComicPage;
use App\Models\Spider;
use App\Models\SpiderPreView;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Psr7\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SpiderController extends Controller
{
    public function index()
    {
        return view("spider");
    }

    public function init()
    {
        return response()->json([
            "history" => [],
        ]);
    }

    //廠商取得並生成資料
    //這邊要建立spider，將抓到的html存到text裡面，第二次以後就不需要抓取跟重新建立資料夾了
    public function info(Request $request)
    {
        $url = $request->input("url",
//            'https://exhentai.org/g/1808392/782ae7115d/'
            "https://exhentai.org/g/1790387/52d73a5ec1/"
        );

        //嘗試取得，否則建立新的spider

        $this->createGuzzle();

        $spider = Spider::where("url", $url)->with("chapter")->first();
        if ($spider) {
            $body = $spider->html;
        } else {

            try {
                $res = $this->client->request('GET', $url);
            } catch (\GuzzleHttp\Exception\GuzzleException $exception) {
                return response()->json([
                    'message' => "輸入有問題",
                    "input"   => $request->all()
                ], 400);
            }

            $body = (string)$res->getBody();
            //清除評論區避免多餘、不必要的資料
            $body = $this->bodyClearComment($body);
        }

        //分解
        $crawler = new Crawler($body);
        //取得封面
        $cover_src   = $crawler->filter('#gd1>div')->first();
        $cover_style = $cover_src->attr("style");
        $cover_403   = $this->getUrlByStyle($cover_style);

        //封面存檔

        if ($spider) {
            $uuid      = $spider->chapter->folder;
            $cover_url = "/storage/comic/{$uuid}.jpg";
        } else {
            $uuid      = Uuid::uuid1()->toString();
            $uuid      = str_replace("-", "", $uuid);
            $cover_url = $this->saveCoverAndDirectory($uuid, $cover_403);
        }

        //取得標題
        $title_en = $crawler->filter("#gn")->first()->text();
        $title    = $crawler->filter("#gj")->first()->text();//雖說是原文，但j可能就是日文的樣子，台灣本也是一樣

        //取得第一頁縮圖
        $gdtm_list = $crawler->filter("#gdt .gdtm>div")
            ->each(function (Crawler $node, $i) {
                return $node->attr("style");
            });

        //計算總共幾頁
        $last_page_number = $crawler->filter(".ptb td:nth-last-child(2) a")->first()->text();

        //模擬每一頁要訪問的網址
        $page_list = [];
        for ($i = 1; $i <= $last_page_number; $i++) {
            $page_list[] = "{$url}?p={$i}";
        }

        $chapter_last_sort = @ComicChapter::orderBy("sort", 'desc')
            ->first()->sort;
        if (!$chapter_last_sort) {
            $chapter_last_sort = 0;
        }
        //如果沒資料則寫入資料庫
        if (!$spider) {

            $chapter           = new ComicChapter();
            $chapter->title    = $title ?? $title_en;
            $chapter->comic_id = ComicChapter::EX_HENTAI;
            $chapter->folder   = $uuid;
            $chapter->sort     = $chapter_last_sort + 1;
            $chapter->save();


            //建立新的spider 記錄
            $new_spider             = new Spider();
            $new_spider->url        = $url;
            $new_spider->html       = $body;
            $new_spider->type_id    = Spider::EXHENTAI;
            $new_spider->chapter_id = $chapter->id;
            $new_spider->save();

            $chapter_id = $chapter->id;
        } else {
            $chapter_id = $spider->chapter->id;
        }


        return response()->json([
            "uuid"       => $uuid,
            "chapter_id" => $chapter_id,
            'target'     => $url,
            'cover'      => $cover_url,

            "title_en"         => $title_en,
            "title"            => $title,
            'first_page_cover' => $gdtm_list,
            'page_number'      => $last_page_number,
            'page_list'        => $page_list,
        ]);

    }

    //取得目錄網址並暫存在spider裡面
    private function getCreatePreviewSpider($url, $chapter_id)
    {
        $spider = Spider::where("url", $url)->with("chapter")->first();
        if ($spider)
            return $spider;

        $this->createGuzzle();

        try {
            $res = $this->client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $exception) {
            return response()->json([
                'message' => "抓取、創建失敗",
            ], 400);
        }
        $body = (string)$res->getBody();
        //清除評論區避免多餘、不必要的資料
        $body = $this->bodyClearComment($body);

        $spider             = new Spider();
        $spider->url        = $url;
        $spider->html       = $body;
        $spider->type_id    = Spider::EXHENTAI;
        $spider->chapter_id = $chapter_id;
        $spider->save();

        return $spider;
    }

    //取得單頁
    public function getPage(Request $request)
    {
        $url        = $request->input("url", 'https://exhentai.org/s/4d79a71985/1851897-5');
        $sort       = $request->input("sort", '5');
        $chapter_id = $request->input("chapter_id", '1');
        $sp         = SpiderPreView::where("url", $url)->first();

        if ($sp) {
            $img_url      = $sp->url;
            $storage_path = $sp->storage_path;
            $storage_url  = $sp->storage_url;
        } else {
            $this->createGuzzle();
            //如果有就直接抓暫存的
            $spider  = Spider::where("url", $url)->first();
            $chapter = ComicChapter::find($chapter_id);

            try {
                $res = $this->client->request('GET', $url);
            } catch (\GuzzleHttp\Exception\GuzzleException $exception) {
                return response()->json([
                    'message' => "輸入有問題",
                    "input"   => $request->all()
                ], 400);
            }

            $body = (string)$res->getBody();

            //分解
            $crawler = new Crawler($body);

            //取得圖片並儲存於本地
            $img_url = $crawler->filter("#img")->attr("src");


            //取得存檔路徑
            $folder = $chapter->folder;
            $uuid   = Uuid::uuid1()->toString();
            $uuid   = str_replace("-", "", $uuid);

            //取得圖片
            $img = $this->client->request('GET', $img_url, ['sink' => "app/public/{$folder}/{$uuid}"]);
            //取得副檔名
            $content_type = $img->getHeader("Content-Type")[0];
            $type         = @explode("/", $content_type)[1];
            //取得圖片內容
            $body = $img->getBody()->getContents();

            $storage_path = storage_path("app/public/comic/{$folder}/{$uuid}.{$type}");
            $storage_url  = url("storage/comic/{$folder}/{$uuid}.{$type}");

            $file = fopen($storage_path, "w");
            fwrite($file, $body);
            fclose($file);


            //寫入資料庫
            $spv               = new SpiderPreView();
            $spv->url          = $url;
            $spv->storage_path = $storage_path;
            $spv->storage_url  = $storage_url;
            $spv->save();

            //寫入page裡面

            $page             = new ComicPage();
            $page->chapter_id = $chapter_id;
            $page->path       = $storage_path;
            $page->url        = $storage_url;
            $page->sort       = $sort;
            $page->save();
        }

        return \response()->json([
            'url' => $storage_url
        ], 200, [], JSON_UNESCAPED_SLASHES);
    }

    //取得預覽圖（？
    public function getPreviewByNumber(Request $request)
    {
        //取得url跟頁數
        $url = $request->input("url",
            'https://exhentai.org/g/1808392/782ae7115d/'
//            "https://exhentai.org/g/1790387/52d73a5ec1/"
        );
        $num = $request->input("num", 0);

        //先看看暫存有沒有記錄資料
        $spider = Spider::where("url", $url)->with('chapter')->first();

        //重取得 html

        if (!$spider) {
            return \response()->json([
                "inp" => $request->all()
            ], 400);
        }

        if ($num > 0) {
            $url    = "{$url}?p={$num}";
            $spider = $this->getCreatePreviewSpider($url, $spider->chapter_id);
        }

        $body = $spider->html;

        $folder  = $spider->chapter->folder;
        $crawler = new Crawler($body);
        //取得第一頁縮圖
        $gdtm_list = $crawler->filter("#gdt .gdtm>div")
            ->each(function (Crawler $node, $i) {
                $sort  = $node->filter("img")->attr("alt");
                $href  = $node->filter("a")->attr("href");
                $style = $node->attr("style");
                return [
                    'sort'  => $sort,
                    'style' => $style,
                    'href'  => $href,
                ];
            });
        //大概長這樣
//margin:1px auto 0; width:100px; height:143px; background:transparent url(https://exhentai.org/m/001790/1790387-00.jpg) -0px 0 no-repeat
        $re = [];
        foreach ($gdtm_list as $value) {
            $item  = $value['style'];
            $_item = @explode("url(", $item);
            $head  = $_item[0];
            $_item = @explode(")", $_item[1]);
            $foot  = $_item[1];

            $_url = $_item[0];

            $s_url = $this->getStorageUrl($_url, $folder);

            $re[] = [
                'sort'  => (int)$value['sort'],
                'style' => $head . "url({$s_url})" . $foot,
                'href'  => $value['href']
            ];
        }

        return \response()->json([
            'number' => $num,
            "list"   => $re
        ], 200, [], JSON_UNESCAPED_SLASHES);
    }

    //清除全部快取
    public function clearPreview()
    {

    }

    //----------------------------------------

    private function bodyClearComment($body)
    {

        $_body = explode('<div id="cdiv" class="gm">', $body);
        $head  = $_body[0];
        $_body = explode('<script type="text/javascript" src="https://exhentai.org/z/0348/ehg_gallery.c.js"></script>', $_body[1]);
        $foot  = $_body[1];
        return $head . $foot;
    }

    private function getUrlByStyle($style)
    {
        //去頭去尾去空白
        $style = @explode("url(", $style)[1];
        $style = @explode(")", $style)[0];

        return trim($style);
    }

    private $preview_storage_url_list = [];

    //取得預覽封面
    private function getStorageUrl($url, $comic_dir)
    {
        $this->createGuzzle();
        //如果有存在就不再抓資料庫
        if (@$this->preview_storage_url_list[$url]) {
            return $this->preview_storage_url_list[$url];
        }

        //爬資料庫
        $get = SpiderPreView::where("url", $url)->first();
        if ($get) {
            $storage_url = $get->storage_url;

            $this->preview_storage_url_list[$url] = $storage_url;

            return $storage_url;
        }
        //如果資料庫不存在就爬取網站並抓取之

        $res = $this->client->request('GET', $url);

        //生成檔案名字
        $file_name    = Uuid::uuid1()->toString();
        $file_name    = str_replace("-", "", $file_name);
        $content_type = $res->getHeader("Content-Type")[0];
        $file_type    = @explode("/", $content_type)[1];
        $path         = storage_path("app/public/comic/{$comic_dir}/{$file_name}.{$file_type}");
        $new_url      = url("storage/comic/{$comic_dir}/{$file_name}.{$file_type}");

        //寫入檔案位置
        $body = $res->getBody()->getContents();
        $file = fopen($path, "w");
        fwrite($file, $body);
        fclose($file);


        //寫入資料庫
        $spv               = new SpiderPreView();
        $spv->url          = $url;
        $spv->storage_path = $path;
        $spv->storage_url  = $new_url;
        $spv->save();

        //寫入暫存
        $this->preview_storage_url_list[$url] = $new_url;

        return $new_url;
    }

    //儲存需要cookie的圖片
    //記得生一個table 方便以後刪掉
    private function saveCoverAndDirectory($name, $url)
    {
        $path       = storage_path("app/public/comic/{$name}");
        $cover_path = storage_path("app/public/comic/{$name}.jpg");

        $res = $this->client->request('GET', $url, ['sink' => $cover_path]);

        $body = $res->getBody()->getContents();

        $file = fopen($cover_path, "w");
        fwrite($file, $body);
        fclose($file);

        //建立資料夾
        File::makeDirectory($path);

        return "/storage/comic/{$name}.jpg";
    }

    /** @var Client $client */
    private $client;

    private function createGuzzle()
    {
        if (!$this->client) {
            $jar = CookieJar::fromArray(//<-------試試cookie
                [
                    "igneous"       => config('exhentai.igneous'),
                    "ipb_member_id" => config('exhentai.ipb_member_id'),
                    "ipb_pass_hash" => config('exhentai.ipb_pass_hash'),
                    "sk"            => config('exhentai.sk'),
                    "sl"            => config('exhentai.sl'),
                ],
                'exhentai.org'
            );

            $this->client = new Client([
                'base_uri' => 'https://exhentai.org/',
                'cookies'  => $jar,
                'stream'   => true,
            ]);

        }

    }

    //我改變主意了，抓到的封面、縮圖都
    public function showImage(Request $request)
    {

        $image = $request->input("url", 'https://exhentai.org/t/0e/08/0e08eab4e568209bcf5459cddd90f65d54130739-5772850-3367-4627-jpg_250.jpg');

        $jar    = CookieJar::fromArray(//<-------試試cookie
            [
                "igneous"       => config('exhentai.igneous'),
                "ipb_member_id" => config('exhentai.ipb_member_id'),
                "ipb_pass_hash" => config('exhentai.ipb_pass_hash'),
                "sk"            => config('exhentai.sk'),
                "sl"            => config('exhentai.sl'),
            ],
            'exhentai.org'
        );
        $client = new Client([
            'base_uri' => 'https://exhentai.org/',
            'cookies'  => $jar,
            'stream'   => true,
        ]);
        $path   = storage_path("app/public/comic/a.jpg");

        $res = $client->request('GET', $image, ['sink' => $path]);

        $body = $res->getBody()->getContents();

//        $imageData = base64_encode($body);
//        echo "data:image/jpeg;base64," . $imageData;

        $response = Response::make($body, 200);
        $response->header("Content-Type", 'image/jpeg');


//        $file = fopen($path, "w");
//        fwrite($file, $body);
//        fclose($file);

        return $response;
    }

    public function test()
    {


//        $url = "https://forum.gamer.com.tw/B.php?bsn=34880";
        $url           = "https://exhentai.org/g/1790387/52d73a5ec1/";
        $igneous       = config('exhentai.igneous');
        $ipb_member_id = config('exhentai.ipb_member_id');
        $ipb_pass_hash = config('exhentai.ipb_pass_hash');
        $sk            = config('exhentai.sk');
        $sl            = config('exhentai.sl');

//        $request_options = [
//            'headers' => [
//                'Cookie' => "igneous={$igneous};"
//                    . "ipb_member_id={$ipb_member_id};"
//                    . "ipb_pass_hash={$ipb_pass_hash};"
//                    . "sk={$sk};"
//                    . "sl={$sl}"
//            ]
//        ];
//        $client = new Client([]);
//  $res = $client->request('GET', $url,$request_options);
        //  echo $res->getBody();

        $url    = 'g/1790387/52d73a5ec1';
        $jar    = CookieJar::fromArray(//<-------試試cookie
            [
                "igneous"       => config('exhentai.igneous'),
                "ipb_member_id" => config('exhentai.ipb_member_id'),
                "ipb_pass_hash" => config('exhentai.ipb_pass_hash'),
                "sk"            => config('exhentai.sk'),
                "sl"            => config('exhentai.sl'),
            ],
            'exhentai.org'
        );
        $client = new Client([
            'base_uri' => 'https://exhentai.org/',
            'cookies'  => $jar
        ]);

        $res = $client->request('GET', $url);
        echo $res->getBody();

    }
}
