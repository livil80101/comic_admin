<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\User;
use App\Services\MatomoService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class TestController extends Controller
{
    public function index(Request $request)
    {
        return view('test.index');
    }

    public function a(Request $request)
    {
        $url = '/test/2';
        echo $request->headers->get('referer');
        echo '<hr/>';
        echo "<a href='{$url}'>go</a>";
    }

    public function b(Request $request)
    {
        $url = '/test/1';
        echo $request->headers->get('referer');
        echo '<hr/>';
        echo "<a href='{$url}'>go</a>";
    }

    public function ajaxTest(Request $request)
    {
        return response()->json([
            'input'  => $request->all(),
            'method' => $request->method(),
        ]);
    }

    public function matomo()
    {
        $page_data = $this->matomoService->getPageDataByUrl("comic");

        dd($page_data);
    }


    //------------------------------------
    //------------------------------------
    public function __construct(MatomoService $matomoService)
    {
        $this->matomoService = $matomoService;
    }

    private $matomoService;
}
