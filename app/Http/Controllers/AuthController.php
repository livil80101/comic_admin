<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //靜態登入頁面
    public function pageLogin()
    {
        return view("login");
    }

    //傳統登入
    public function loginByPasswordStatic(Request $request)
    {
        $account  = $request->input("account");
        $password = $request->input("password");


        $em = Employee::where("account", $account)->first();

        if (!($em && Hash::check($password, $em->password))) {
            Auth::logout();
            return redirect(route("login.static"))->withErrors(["帳密不對"]);
        }

        Auth::login($em);
        return redirect(route("root"));
    }

    //傳統登出
    public function logoutStatic()
    {
        Auth::logout();
        return redirect("login.static");
    }


    //
    public function checkUser()
    {
        return response()->json([
            "user" => Auth::user()
        ]);
    }

    //ajax 登入
    public function login(Request $request)
    {
        $account  = $request->input("account");
        $password = $request->input("password");

        $em = Employee::where("account", $account)->first();
        if (!($em && Hash::check($password, $em->password))) {
            Auth::logout();
            return response()->json([
                "message" => '登入失敗'
            ], 400);
        }

        Auth::login($em);
        event('user.login', $em);

        return response()->json([
            "user" => $em,
        ]);
    }

    //ajax 登出
    public function logout()
    {
        Auth::logout();
        return response()->json([]);
    }

    public function test()
    {

        dd(
            "你的帳號是",
            Auth::user(),
            "登出"

        );
    }
}
