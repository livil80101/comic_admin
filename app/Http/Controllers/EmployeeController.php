<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\EmployeeData;
use App\Models\Permission;
use App\Models\Role;
use App\Repositories\RoleRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class EmployeeController extends Controller
{
    //身份
    public function roles()
    {
        $roles = Role::with([
            'permissions' => function (BelongsToMany $q) {
                $q->select("id");
            }
        ])->where("type", Role::EMPLOYEE)->get()->mapWithKeys(function ($item) {
            return [$item->id => $item];
        });

        $permissions = $this->roleRepository->getAllEmployeePermissions();

        return response()->json([
            'roles'       => $roles,
            'permissions' => $permissions
        ]);
    }

    //新增
    public function addRole(Request $request)
    {
        $display_name = $request->input("display_name");
        $name         = $request->input("name");

        $has = Role::where(function ($q) use ($display_name, $name) {
            $q->where("display_name", $display_name)->orWhere("name", $name);
        })->where("type", Role::EMPLOYEE)->get();

        if ($has->count() > 0) {
            return response()->json([
                'input'   => $request->all(),
                'message' => "已存在資料"
            ], 400);
        }

        $role               = new Role();
        $role->display_name = $display_name;
        $role->name         = $name;
        $role->description  = "";
        $role->type         = Role::EMPLOYEE;
        $role->save();

        return response()->json([
            'role'  => $role,
            'roles' => $this->roleRepository->getAllEmployeeRoles(1, 1),
            'input' => $request->all(),
        ]);
    }

    //修改身分權限
    public function updateRole(Request $request, $id)
    {
        $role = Role::where("id", $id)->first();
        if (!$role) {
            return response()->json([
                "id"      => $id,
                'input'   => $request->all(),
                'message' => "未發現指定的id"
            ], 404);
        }

        $pid         = $request->input("pid");
        $description = $request->input("description");

        $permissions = $this->roleRepository->getAllEmployeePermissions();
        $check_id    = $permissions->pluck('id')->toArray();

        $check = array_diff($pid, $check_id);
        if (count($check) > 0 && $role->name != "ADMIN") {
            return response()->json([
                "id"      => $id,
                'input'   => $request->all(),
                'message' => "錯誤的資料"
            ], 400);
        }

        $role->permissions()->sync($pid);
        $role->description = $description;
        $role->save();

        return response()->json([
            'id'    => $id,
            'input' => $request->all(),
        ]);
    }

    public function deleteRole(Request $request, $id)
    {
        $role = Role::where("id", $id)->first();
        if (!$role) {
            return response()->json([
                "id"      => $id,
                'message' => "未發現指定的id"
            ], 404);
        }

        //刪除，看要不要event做記錄
        $role->delete();

        return response()->json([
            'id'    => $id,
            'roles' => $this->roleRepository->getAllEmployeeRoles(1, 1),
        ]);
    }

    //一般員工列表
    public function employeeList(Request $request)
    {
        $name    = $request->input('name');
        $account = $request->input('account');
        $email   = $request->input('email');
        $limit   = $request->input("limit", 30);

        $emp = Employee::orderBy("employees.id")
            ->leftJoin("employee_data", "employee_data.id", "=", "employees.id")
            ->select(
                "employees.id",
                "employees.email",
                "employees.account",
                "employee_data.name",
            )->
            with(['roles']);

        $searched  = Session::get("searched", []);
        $_searched = [];

        if ($name && strlen($name) > 0) {
            $_searched['name'] = $name;

            $emp = $emp->where("employee_data.name", "like", "%{$name}%");
        }

        if ($account && strlen($account) > 0) {
            $_searched['account'] = $account;

            $emp = $emp->where("employees.account", "like", "%{$account}%");
        }

        if ($email && strlen($email) > 0) {
            $_searched['email'] = $email;

            $emp = $emp->where("employees.email", "like", "%{$email}%");
        }

        if ($_searched) {
            if (count($searched) > 5) {
                array_pop($searched);
            }
            array_unshift($searched, $_searched);
            Session::put('searched', $searched);
        }


        $emp = $emp->paginate($limit);

        return response()->json([
            'list'            => $emp,
            'searched'        => $searched,
            'access_employee' => Session::get("access_employee", [])
        ]);
    }

    //取得全部員工的資料
    public function employeeDataList(Request $request)
    {
        //取得全部員工
        $em = Employee::where('enable', true)
            ->select("id", "account", "email")
            ->with([
                'data',
                'roles',
            ])->get()->mapWithKeys(function ($item) {
                return [$item->id => $item];
            });

        //取得身份
        $roles = $this->roleRepository->getAllEmployeeRoles();

        return response()->json([
            'employees' => $em->toarray(),
            "sex"       => EmployeeData::getAllSex(),
            "blood"     => EmployeeData::getAllBlood(),
            "roles"     => $roles,
        ]);
    }

    //修改員工細項
    public function updateEmployeeDetail(Request $request, $id)
    {
        $all = $request->all();
        $em  = Employee::where("id", $id)->first();

        if (!$em) {
            return response()->json([
                "id"      => $id,
                'input'   => $request->all(),
                'message' => "使用者不存在"
            ], 404);
        }

        //檢查帳號是否重複
        $email   = @$all["email"];
        $account = @$all["account"];

        $check1 = Employee::where("id", '<>', $id)
            ->where(function ($query) use ($email, $account) {
                $query->where('email', $email)->orWhere('account', $account);
            })
            ->get();

        if ($check1->count() > 0) {
            //如果真的要仔細點出有誰重複就用迴圈展開check

            return response()->json([
                "id"      => $id,
                'input'   => $request->all(),
                'message' => "帳號、email已經有人使用"
            ], 400);
        }

        $eid = @$all["eid"];
        $pid = @$all["pid"];

        $check2 = EmployeeData::where("id", '<>', $id)
            ->where(function ($query) use ($eid, $pid) {
                $query->where('eid', $eid)->orWhere('pid', $pid);
            })
            ->get();
        if ($check2->count() > 0) {
            //如果真的要仔細點出有誰重複就用迴圈展開check

            return response()->json([
                "id"      => $id,
                'input'   => $request->all(),
                'message' => "身分證字號、員工編號重複"
            ], 400);
        }

        //生日部分之後再來寫比較嚴謹的判定
        $birthday = @$all["birthday"];

        if ($birthday && !strtotime($birthday)) {
            return response()->json([
                "id"      => $id,
                'input'   => $request->all(),
                'message' => "生日格式錯誤"
            ], 400);
        }

        //驗證血型

        //驗證性別

        //驗證姓名

        //修改資料
        //這邊就採用信賴上面的資料
        Employee::where("id", $id)->update([
            'account' => $account,
            'email'   => $email,
        ]);
        EmployeeData::where("id", $id)->update([
            "eid"      => $eid,
            "pid"      => $pid,
            "birthday" => $birthday,
            'name'     => $all["name"],
            'address'  => $all["address"] ?? "",
            'sex'      => @$all["sex"],
            'blood'    => @$all["blood"],
            'tel'      => $all["tel"] ?? "",
        ]);

        return response()->json([
            'id'    => $id,
            'input' => $request->all(),
            'db'    => 1,
        ]);
    }

    //修改員工身份
    public function updateEmployeeRoles(Request $request, $id)
    {
        $em = Employee::where("id", $id)->first();
        if (!$em) {
            return response()->json([
                "id"      => $id,
                'input'   => $request->all(),
                'message' => "使用者不存在"
            ], 404);
        }
        //注意一些特例的帳號，不會被修改到資料，跳403
        //比方說 hr 不能 拿掉 grand_hr 的權限

        //記得驗證

        $_r = Role::where('type', Role::EMPLOYEE)->get();
        $r  = $_r->pluck('id')->toArray();

        $roles = $request->input("roles", []);

        //判定是否有漏網之魚
        $check = array_diff($roles, $r);
        if (count($check) > 0) {
            return response()->json([
                "id"      => $id,
                'input'   => $request->all(),
                'message' => "錯誤的資料"
            ], 400);
        }

        //改變權限後記得記錄一下有誰動到誰，避免起爭議
        $em->roles()->sync($roles);

        return response()->json([
            'id'    => $id,
            'input' => $request->all(),
            'db'    => 1,
        ]);
    }

    public function employeeData(Request $request, $id)
    {
        $data = EmployeeData::where("id", $id)
            ->first();

        //沒資料就炸掉
        if (!$data) {
            return response()->json([], 404);
        }

        $access_employee = Session::get("access_employee", []);


        if (count($access_employee) > 5) {
            array_pop($access_employee);
        }
        array_unshift($access_employee, $data);
        Session::put('access_employee', $access_employee);

        //取得身份
        $roles = $this->roleRepository->getAllEmployeeRoles();

        return response()->json([
            'data'  => $data,
            'roles' => $roles,
            "sex"   => EmployeeData::getAllSex(),
            "blood" => EmployeeData::getAllBlood(),
        ]);
    }

    public function employeeDataEdit(Request $request)
    {
        $all_data = $request->all();

        $update_data   = [];
        $error_message = [];

        //修改姓名
        if (@$all_data["name"]) {
            $name_check = strlen($all_data["name"]) > 0;

            if ($name_check) {
                $update_data["name"] = $all_data['name'];
            }
        }

        //修改性別
        if (@$all_data["sex"]) {
            $sex = $all_data["sex"];

            $all_sex  = EmployeeData::getAllSex();
            $sex_code = array_column($all_sex, 'code');

            if (in_array($sex, $sex_code)) {
                $update_data["sex"] = $all_data['sex'];
            } else {
                $error_message[] = "性別：無效的參數";
            }
        }

        //血型
        if (@$all_data["blood"]) {
            $blood = $all_data["blood"];

            $all_blood  = EmployeeData::getAllBlood();
            $blood_code = array_column($all_blood, 'code');

            if (in_array($blood, $blood_code)) {
                $update_data["blood"] = $all_data['blood'];
            } else {
                $error_message[] = "血型：無效的參數";
            }
        }

        return response()->json([
            "update_data"   => $update_data,
            "error_message" => $error_message,
            "input"         => $all_data,
        ]);
    }

    public function createEmployee(Request $request)
    {
        $e           = new Employee();
        $e->password = config("employee.default_password");

        $ed = new EmployeeData();

        return response()->json([
            'a' => 1
        ]);
    }

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    private $roleRepository;
}
