<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        return view("default");
    }

    public function csrf(Request $request)
    {
        return response()->json([
            "name"  => "_token",
            "token" => csrf_token(),
        ]);
    }
}
