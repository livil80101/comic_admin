<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string|null $role 身份
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $role = null)
    {

        if ($role) {
//            dd("中介層" . $role . "判定還沒寫完");
        }
        return $next($request);
    }
}
