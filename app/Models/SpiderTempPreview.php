<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SpiderTempPreview
 *
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderTempPreview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderTempPreview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderTempPreview query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $spider_id
 * @property string $style 縮圖顯示
 * @property string $url 抓取對象
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderTempPreview whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderTempPreview whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderTempPreview whereSpiderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderTempPreview whereStyle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderTempPreview whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderTempPreview whereUrl($value)
 */
class SpiderTempPreview extends Model
{
    protected $table = 'spider_temp_preview';
    use HasFactory;
}
