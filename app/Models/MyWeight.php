<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\MyWeight
 *
 * @property int $id
 * @property string $weight 體重
 * @property int $absorb_calories 吸收熱量
 * @property int $consume_calories 消耗熱量
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|MyWeight newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MyWeight newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MyWeight query()
 * @method static \Illuminate\Database\Eloquent\Builder|MyWeight whereAbsorbCalories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MyWeight whereConsumeCalories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MyWeight whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MyWeight whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MyWeight whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MyWeight whereWeight($value)
 * @mixin \Eloquent
 */
class MyWeight extends Model
{
    use HasFactory;

    protected $table = 'my_weight';
    protected $primaryKey = 'id';
}
