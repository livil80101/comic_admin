<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ComicChapter
 *
 * @property int $id
 * @property string $title
 * @property int $comic_id
 * @property string $folder 放檔案的資料夾名稱
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ComicChapter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ComicChapter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ComicChapter query()
 * @method static \Illuminate\Database\Eloquent\Builder|ComicChapter whereComicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicChapter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicChapter whereFolder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicChapter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicChapter whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicChapter whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicChapter whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ComicPage[] $pages
 * @property-read int|null $pages_count
 */
class ComicChapter extends Model
{
    use HasFactory;

    protected $table = 'comic_chapters';
    const EX_HENTAI = 2;

    public function pages()
    {
        return $this->hasMany(ComicPage::class, 'chapter_id', 'id');
    }
}
