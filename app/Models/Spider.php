<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Spider
 *
 * @property int $id
 * @property int $chapter_id
 * @property string $url
 * @property string $html
 * @property int $type_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\ComicChapter $chapter
 * @method static \Illuminate\Database\Eloquent\Builder|Spider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Spider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Spider query()
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereChapterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Spider whereUrl($value)
 * @mixin \Eloquent
 */
class Spider extends Model
{
    use HasFactory;

    protected $table = 'spiders';

    const EXHENTAI = 1;

    public function chapter()
    {
        return $this->belongsTo(\App\Models\ComicChapter::class, 'chapter_id', 'id');
    }
}
