<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Comic
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Comic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comic newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Comic query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $author_id
 * @property string $description
 * @property string $cover
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereUpdatedAt($value)
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereTitle($value)
 * @property int $enable
 * @method static \Illuminate\Database\Eloquent\Builder|Comic whereEnable($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ComicChapter[] $chapters
 * @property-read int|null $chapters_count
 */
class Comic extends Model
{
    use HasFactory;

    public $table = 'comic';

    public function chapters()
    {
        return $this->hasMany(ComicChapter::class, 'comic_id', 'id');
    }
}
