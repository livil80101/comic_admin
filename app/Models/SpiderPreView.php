<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SpiderPreView
 *
 * @property int $id
 * @property string $url
 * @property string $storage_url
 * @property string $storage_path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderPreView newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderPreView newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderPreView query()
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderPreView whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderPreView whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderPreView whereStoragePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderPreView whereStorageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderPreView whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SpiderPreView whereUrl($value)
 * @mixin \Eloquent
 */
class SpiderPreView extends Model
{
    use HasFactory;

    protected $table = "spider_preview";
}
