<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmployeeData
 *
 * @property int $id 跟員工id同樣
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData query()
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $enable 是否開通
 * @property string|null $avatar 頭像
 * @property string|null $pid 身分證字號
 * @property string|null $eid 員工識別代號
 * @property int|null $sex 性別
 * @property string|null $birthday 生日
 * @property int|null $blood 血型
 * @property string|null $tel 聯絡電話
 * @property string $address 地址
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereBlood($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereEid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereEnable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData wherePid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|EmployeeData whereTel($value)
 */
class EmployeeData extends Model
{
    use HasFactory;

    const BLOOD_NULL = null;
    const BLOOD_A = 1;
    const BLOOD_B = 2;
    const BLOOD_AB = 3;
    const BLOOD_O = 4;

    public static function getAllBlood()
    {
        return [
            [
                "name" => '未填寫',
                "code" => self::BLOOD_NULL,
            ],
            [
                "name" => 'A型',
                "code" => self::BLOOD_A,
            ],
            [
                "name" => 'B型',
                "code" => self::BLOOD_B,
            ],
            [
                "name" => 'AB型',
                "code" => self::BLOOD_AB,
            ],
            [
                "name" => 'O型',
                "code" => self::BLOOD_O,
            ],
        ];
    }

    const SEX_NULL = null;
    const SEX_MALE = 1;
    const SEX_FEMALE = 2;

    /**
     * 取得性別列表
     */
    public static function getAllSex()
    {
        return [
            [
                "name" => '未填寫',
                "code" => self::SEX_NULL,
            ],
            [
                "name" => '男',
                "code" => self::SEX_MALE,
            ],
            [
                "name" => '女',
                "code" => self::SEX_FEMALE,
            ],
        ];
    }
}
