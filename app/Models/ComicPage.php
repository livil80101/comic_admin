<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ComicPage
 *
 * @property int $id
 * @property int $chapter_id
 * @property string $path
 * @property string $url
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ComicPage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ComicPage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ComicPage query()
 * @method static \Illuminate\Database\Eloquent\Builder|ComicPage whereChapterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicPage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicPage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicPage wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicPage whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicPage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComicPage whereUrl($value)
 * @mixin \Eloquent
 */
class ComicPage extends Model
{
    protected $table = 'comic_pages';
    use HasFactory;
}
