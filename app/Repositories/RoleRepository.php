<?php


namespace App\Repositories;


use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * 身份跟權限都集中在這裡
 */
class RoleRepository
{

    /**
     * 取得全部員工身份
     *
     * @param boolean $withAdmin
     * @param boolean $withRole
     *
     * @return array
     */
    public function getAllEmployeeRoles($withAdmin = false, $withRole = false)
    {
        $re = Role::where("type", Role::EMPLOYEE);

        if (!$withAdmin) {
            $re = $re->where('name', '<>', 'ADMIN');
        }

        if ($withRole) {
            $re = $re->with([
                'permissions' => function (BelongsToMany $q) {
                    $q->select("id");
                }
            ]);
        }

        return $re->get()->mapWithKeys(function ($item) {
            return [$item->id => $item];
        })->toArray();
    }


    /**
     * 取得全部員工權限
     */
    public function getAllEmployeePermissions()
    {
        return Permission::where("type", Permission::EMPLOYEE)->get();
    }
}
