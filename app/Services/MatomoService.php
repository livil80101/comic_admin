<?php


namespace App\Services;

use Illuminate\Support\Facades\Session;

/**
 * Matomo 相關功能集中
 *
 * API 參考這篇 https://developer.matomo.org/api-reference/reporting-api
 * 要不是php版本差太多，不然會想用這個 https://github.com/VisualAppeal/Matomo-PHP-API/blob/master/src/Matomo.php
 *
 */
class MatomoService
{
    /**
     * 設定使用者編號
     */
    public function setUserId($id = null)
    {
        if (!$this->matomo)
            return false;
        if (!$id)
            $id = Session::get("user_id");

        $this->matomo->setUserId($id);

        return true;
    }

    /**
     * 追蹤事件
     */
    public function setTrackEvent($category, $action, $name = false, $value = false)
    {
        if (!$this->matomo)
            return false;

        $this->matomo->doTrackEvent($category, $action, $name, $value);

        return true;
    }

    /**
     * 取得搜尋關鍵字列表
     *
     * "label" => "創"
     * "nb_visits" => 2 //搜尋數
     * "nb_hits" => 2 //結果
     * "sum_time_spent" => 5 // 花費時間（微秒？
     * "exit_nb_visits" => "1" //停留時間？
     * "nb_pages_per_search" => 1 //每次搜尋幾頁
     * "avg_time_on_page" => 3 //
     * "bounce_rate" => "0%"
     * "exit_rate" => "50%"
     * "segment" => "siteSearchKeyword==%E5%89%B5"
     */
    public function getSearchKeywords()
    {
        $data = [
            "module"       => "API",
            "method"       => "Actions.getSiteSearchKeywords",
            "idSite"       => $this->id,
            "period"       => $this->period,
            "date"         => $this->date,
            "format"       => "JSON",
            "filter_limit" => 100,
            "token_auth"   => $this->token,
        ];

        $q   = http_build_query($data);
        $url = $this->url . "?" . $q;

        return $this->getUrlData($url);
    }

    public function getDownload($label = '')
    {
        $method = 'Actions.getDownloads';
//        $this->setDate("2021-06-27");
        $data = [
            "module"       => "API",
            "method"       => $method,
//            "idSubtable"   => 1,
            "idSite"       => $this->id,
            "period"       => $this->period,
            "date"         => $this->date,
            "format"       => "JSON",
            "filter_limit" => 100,
            "token_auth"   => $this->token,
        ];

        $q   = http_build_query($data);
        $url = $this->url . "?" . $q;

        $download_list = $this->getUrlData($url);

        $idSubtable = null;
        foreach ($download_list as $item) {
            if ($item['label'] == $label) {
                $idSubtable = $item['idsubdatatable'];
                break;
            }
        }

        if (!$idSubtable) {
            return null;
        }


        $data["idSubtable"] = $idSubtable;

        $q   = http_build_query($data);
        $url = $this->url . "?" . $q;

        return $this->getUrlData($url);
    }

    /**
     * 取得指定頁面資料
     */
    public function getPageDataByUrl($kw)
    {
        //嘗試取得全部網址資料
        $method = "Actions.getPageUrls";
        $data   = [
            "module"       => "API",
            "method"       => $method,
            "idSite"       => $this->id,
            "period"       => $this->period,
            "date"         => $this->date,
            "format"       => "JSON",
            "filter_limit" => 100,
            "token_auth"   => $this->token,
        ];
        $q      = http_build_query($data);
        $url    = $this->url . "?" . $q;

        $url_data = $this->getUrlData($url);

        //比對是否有指定的網址
        $idSubtable = null;
        foreach ($url_data as $datum) {
            if ($datum['label'] == $kw) {
                $idSubtable = $datum['idsubdatatable'];
            }
        }
        if (!$idSubtable) {
            return null;
        }

        //有則以該id獲取資料
        $data["idSubtable"] = $idSubtable;

        $q   = http_build_query($data);
        $url = $this->url . "?" . $q;

        return $this->getUrlData($url);
    }

    /**
     * 取得全部事件行為
     */
    public function getAllAction()
    {
        $method = "Events.getAction";
        $data   = [
            "module"       => "API",
            "method"       => $method,
            "idSite"       => $this->id,
            "period"       => $this->period,
            "date"         => $this->date,
            "format"       => "JSON",
            "filter_limit" => 100,
            "token_auth"   => $this->token,
        ];

        $q   = http_build_query($data);
        $url = $this->url . "?" . $q;

        return $this->getUrlData($url);
    }

    //透過
    public function getEventsActionByCategoryId($id)
    {
        $method = 'Events.getActionFromCategoryId';
        $data   = [
            "module"       => "API",
            "method"       => $method,
            "idSubtable"   => $id,
            "idSite"       => $this->id,
            "period"       => $this->period,
            "date"         => $this->date,
            "format"       => "JSON",
            "filter_limit" => 1000,
            "token_auth"   => $this->token,
        ];
        $q      = http_build_query($data);
        $url    = $this->url . "?" . $q;

        return $this->getUrlData($url);
    }

    //透過
    public function getNameByActionId($id)
    {
        $method = 'Events.getNameFromActionId';
        $data   = [
            "module"       => "API",
            "method"       => $method,
            "idSubtable"   => $id,
            "idSite"       => $this->id,
            "period"       => $this->period,
            "date"         => $this->date,
            "format"       => "JSON",
            "filter_limit" => 1000,
            "token_auth"   => $this->token,
        ];
        $q      = http_build_query($data);
        $url    = $this->url . "?" . $q;

        return $this->getUrlData($url);
    }

    private function getUrlData($url)
    {
        $headerArray = array("Content-type:application/json;", "Accept:application/json");
        $ch          = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output, true);
    }

    /**
     * 可以用
     * "method"       => "Actions.getPageTitle",
     * "pageName"       => "-----",
     * 來指定標題
     */
    public function test()
    {
        $method = "Events.getCategory";
        $this->setDate("2021-06-27");
        $data = [
            "module"       => "API",
            "method"       => $method,
//            "pageUrl"      => "show_image/1",
            "idSubtable"   => 6,
            "idSite"       => $this->id,
            "period"       => $this->period,
//            "date"         => "today",
            "date"         => $this->date,
            "format"       => "JSON",
            "filter_limit" => 100,
            "token_auth"   => $this->token,
        ];

        $q   = http_build_query($data);
        $url = $this->url . "?" . $q;
        dd(
            $method, $this->getUrlData($url)
        );
        return $this->getUrlData($url);
    }


    public function __construct()
    {
        $this->id    = config("matomo.id");
        $this->url   = config("matomo.url");
        $this->token = config("matomo.token");

        $this->date   = date("Y-m-d");
        $this->period = "month";

        $this->matomo = new \MatomoTracker((int)$this->id, $this->url);
    }

    /**
     * @param false|string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @param string $period
     */
    public function setPeriod(string $period)
    {
        $this->period = $period;
    }

    private $id;
    private $url;
    private $token;
    private $matomo;
    private $date;
    private $period;
}
