<?php

namespace App\Listeners;

use App\Models\Employee;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Session;

/**
 * 登入後記錄session的地方
 */
class UserLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param Employee $event
     * @return void
     */
    public function handle($event = null)
    {
//        Session::put("a", "a");
    }
}
