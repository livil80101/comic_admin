<?php

namespace Database\Seeders;


use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class UserDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $user = User::find(2);
//        $user->roles()->attach(2);


    }

    private function defaultRole()
    {
        $user_ids = User::all()->pluck('id')->toArray();
        $add      = [];
        foreach ($user_ids as $user_id) {
            $add[] = [
                'user_id' => $user_id,
                'role_id' => 2,
            ];
        }

        DB::table('user_role')->insert($add);
    }

    //寫入角色
    private function setRole()
    {
        $roles = [
            [
                'id'           => 1,
                'name'         => 'ADMIN',
                'display_name' => '管理員',
                'description'  => '管理員',
            ], [
                'id'           => 2,
                'name'         => 'GUEST',
                'display_name' => '一般使用者',
                'description'  => '一般使用者',
            ], [
                'id'           => 3,
                'name'         => 'ADULT',
                'display_name' => '成年人',
                'description'  => '成年人',
            ],
        ];
        foreach ($roles as $role) {
            $r               = new Role();
            $r->id           = $role['id'];
            $r->name         = $role['name'];
            $r->display_name = $role['display_name'];
            $r->description  = $role['description'];
            $r->save();
        }
    }

    //寫入權限
    private function setPermissions()
    {
        $permissions = [
            //USER類
            [
                'name'         => 'ADD_USER',
                'display_name' => '新增使用者',
                'description'  => '新增使用者',
            ], [
                'name'         => 'UPDATE_USER',
                'display_name' => '改變使用者',
                'description'  => '改變使用者（包含軟刪除）',
            ], [
                'name'         => 'ROLE',
                'display_name' => '會員種類管理',
                'description'  => '設定會員與對應身份的功能',
            ], [
                'name'         => 'PERMISSION',
                'display_name' => '設定權限',
                'description'  => '設定權限',
            ],
            //漫畫
            [
                'name'         => 'ADD_COMIC',
                'display_name' => '新增漫畫',
                'description'  => '新增漫畫',
            ], [
                'name'         => 'UPDATE_COMIC',
                'display_name' => '更改漫畫',
                'description'  => '更改漫畫',
            ], [
                'name'         => 'DELETE_COMIC',
                'display_name' => '刪除漫畫',
                'description'  => '刪除漫畫',
            ], [
                'name'         => 'ADULT',
                'display_name' => '成年人',
                'description'  => '成年人',
            ],
            //GUEST
            [
                "name"         => "SUBSCRIPTION",
                'display_name' => '訂閱',
                "description"  => "訂閱相關功能權限",
            ],
        ];
        foreach ($permissions as $permission) {
            $p               = new Permission();
            $p->name         = $permission["name"];
            $p->display_name = $permission["display_name"];
            $p->description  = $permission["description"];
            $p->save();
        }
    }

    //取資料並寫入 user
    private function loadCSV()
    {
        $raw_path = storage_path("app/user_data.csv");

        $row = File::get($raw_path);

        $arr = explode("\r\n", $row);
        foreach ($arr as $key => $item) {
            if ($key == 0) continue;

            $data = explode(',', $item);

            //1100001,H295125143,郭家虹,桃園市平鎮區新貴里17鄰關爺北路140巷8號1樓,20200001,(05)6626939
            $account = @$data[1];
            $name    = @$data[2];

            $user           = new User();
            $user->name     = $name;
            $user->email    = $account . "@gmail.com";
            $user->password = Hash::make($account);
            $user->save();

            echo "新增{$name}\n";
        }
    }
}
