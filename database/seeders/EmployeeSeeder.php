<?php

namespace Database\Seeders;

use App\Models\Employee;
use App\Models\EmployeeData;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //初始帳號
        $em           = new Employee();
        $em->account  = config("account.root_account");
        $em->password = Hash::make(config("account.root_password"));
        $em->email    = config("account.root_email");
        $em->save();

        $ed       = new EmployeeData();
        $ed->id   = $em->id;
        $ed->name = "開發人員帳號";
        $ed->pid  = "A123456789";
        $ed->eid  = "0000000000";
        $ed->save();

        $em           = new Employee();
        $em->account  = "grand_hr";
        $em->password = Hash::make('grand_hr');
        $em->email    = "lee.two.800110@gmail.com";
        $em->save();

        $ed       = new EmployeeData();
        $ed->id   = $em->id;
        $ed->name = "HR帳號";
        $ed->pid  = "A12345678";
        $ed->eid  = "0000000001";
        $ed->save();

        //初始帳號
        $raw_path = storage_path("app/user_data.csv");

        $row = File::get($raw_path);

        $arr = explode("\r\n", $row);

        $eid = date("Ymd000");
        foreach ($arr as $key => $item) {
            $eid++;
            if ($key == 0) continue;

            $data = explode(',', $item);

            //1100001,H295125143,郭家虹,桃園市平鎮區新貴里17鄰關爺北路140巷8號1樓,20200001,(05)6626939
            $account = @$data[1];
            $name    = @$data[2];
            $address = @$data[3];
            $tel     = @$data[5];

            $em           = new Employee();
            $em->account  = $account;
            $em->password = Hash::make($account);
            $em->email    = $account . "@gmail.com";
            $em->save();

            $ed          = new EmployeeData();
            $ed->id      = $em->id;
            $ed->name    = $name;
            $ed->pid     = $account;
            $ed->eid     = $eid;
            $ed->address = $address;
            $ed->tel     = $tel;
            $ed->save();

            echo "新增{$name}\n";
        }

    }
}
