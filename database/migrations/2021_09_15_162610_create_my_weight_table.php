<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMyWeightTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_weight', function (Blueprint $table) {
            $table->id();
            $table->decimal('weight')->comment("體重");
            $table->unsignedInteger('absorb_calories')->comment('吸收熱量');
            $table->unsignedInteger('consume_calories')->comment('消耗熱量');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_weight');
    }
}
