const mix   = require('laravel-mix');
let webpack = require('webpack');
const path  = require('path');

mix.webpackConfig({
    output: {
        // publicPath: "public",
        path: path.resolve(__dirname, 'public'),

    },

    plugins: [
        new webpack.ProvidePlugin({
            $              : "jquery",
            jQuery         : "jquery",
            jquery         : "jquery",
            "window.jQuery": "jquery",

            _: "lodash",

            axios: "axios",
            Vue  : "vue",
        })
    ],

    // module: {
    //     rules: [
    //         {
    //             test: /\.(eot|svg|ttf|woff|woff2?)$/,
    //             loader: 'url-loader'
    //         }
    //     ]
    // },
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js/app.js')//route用
    .js('resources/js/spider.js', 'public/js/spider.js')
    // .js('resources/js/login.js', 'public/js')//針對登入用
    .sass('resources/css/app.scss', 'public/css/app.css')
    .extract(['vue'])
;
